export default (call, params) => {
	// Analagous to form.parse
	const url = params[0];

	// Do specific thing with the result of the parsed command
	return call('registerMastodon', [url]);
};
