import JsonRpc from 'node-jsonrpc-client';

function parse(args=process.argv.slice(2)){
const props = {};
const params = [];

for(let i=0; i < args.length; i++){
	const m = args[i].match(/^-(.)(?:(?=.)=?(.*))?$/i)
	if(m){
		const key = m[1]
		const value = m[2] ?? args[++i];
		props[key] = value;
	} else {
		params.push(args[i]);
	}
}

return [params, props]
}

export default function(args) {
	const [params, props] = parse(args);

	const port = props.p || 8081;

const client = new JsonRpc('http://localhost:'+port+'/');

async function call(task, args){
	const res = await client.call(task, args);

	if(res.error){
		console.error('Error:', res.error.data || res.error.message);
		process.exit(1);
	} else {
		console.log(res.result);
	}
}

return method => method(call, params, props);
}
