import start from 'simple-wsgi';
import rpc from './admin-server.js';
import main, {admin} from './index.js';
import fs from 'fs';
import db from 'sqlite-execute-tag';

const port = process.env.PORT || 8080;
const adminPort = process.env.ADMIN_PORT || (parseInt(port,10)+1);
const origin = process.env.ORIGIN || 'http://localhost:' + port;

const adminApp = admin({origin})

async function getClient(){
	try {
		return (JSON.parse(fs.readFileSync('./client.json', 'utf-8')));
	} catch(e) {
		console.log(e);
		const client = await adminApp.registerMastodon();
		fs.writeFileSync('./client.json', JSON.stringify(client));
		return client.toJSON();
	}
}

const app = await main({
	origin,
	authClient: await getClient(),
	jwtKey: 'secret',
	sql: db('dev.sqlite')
});

start(app, port);
start(rpc(adminApp), adminPort);
