import ui from './ui/index.js';
import Admin from './admin/methods.js';
import Auth from 'masto-auth';

const title = 'Directory';
const authPath = '/auth';

const app = async ({authClient, jwtKey, sql}) => {
	const u = await ui({
		title,
		auth: new Auth(origin=>{
			if(origin === 'https://kith.kitchen'){
				return authClient;
			}
		}),
		authPath,
		jwtKey,
		sql
	});

	const router = async (req, res) => {
		return u(req, res);
	};

	return router;
};

export default app;
export const admin = options => Admin(Object.assign({title, authPath}, options));
