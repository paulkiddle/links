import html from 'encode-html-template-tag';
import form from 'html-form-component';
import { redirect } from '../../rsvp/helpers.js';

class ValidationError extends Error {}

function validateTags(submitted, allowed) {
	if(submitted.some(value=>!allowed.includes(parseInt(value, 10)))){
		throw new ValidationError('Select tags from the tag list.');
	}
	return submitted;
}

const AddLink = tags => form(
	{
		method: 'POST'
	},
	html`<label>
		Name
		<input name="title">
	</label><br>
	<label>
		URL
		<input name="href" type="url">
	</label><br>
	<label>
		Description
		<textarea name="description"></textarea>
	</label>
	<label>
		Tags
		<select multiple name="tags">
		${tags.map(t=>html`<option value="${t.id}">${t.id} ${t.title}</option>`)}
		</select>
	</label>
	<button>Submit</button>
	`,
	params => ({
		title: params.get('title'),
		href: params.get('href'),
		description: params.get('description'),
		tags: validateTags(params.getAll('tags'), tags.map(t=>t.id))
	})
);

export default async (req, user) => {
	const tags = await user.getTags();
	const addLink = AddLink(tags);
	const errors = [];

	if(req.method === 'POST') {
		try {
			await user.addLink(await addLink.parse(req));
			return redirect('/');
		} catch(e) {
			if(e instanceof ValidationError) {
				errors.push(e);
			} else {
				throw e;
			}
		}
	}

	return html`${errors}${addLink}`;
};
