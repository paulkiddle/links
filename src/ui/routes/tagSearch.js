import html from 'encode-html-template-tag';

const tags = tags => tags.map(tag=>html`<a href="/tags/${tag.title}">${tag.title}</a>`);

export default async (req, user, tag) => {
	return (await user.getLinks({tag})).map(async l=>html`<article>
		<h1><a href="${l.url}">${l.title}</a></h1>
		<p>${l.description}</p>
		<p>${tags(await user.getTagsFor(l.id))}</p>
	</article>`);
};
