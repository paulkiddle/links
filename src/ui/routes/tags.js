import html from 'encode-html-template-tag';
import form, {parser} from 'html-form-component';

const addTag = tags => form(
	{
		method: 'POST'
	},
	html`<label>
		Name
		<input name="title">
	</label><br>
	<label>
		Parent
		<select name="parent">
		<option></option>
		${tags.map(tag=>html`<option>${tag.title}</option>`)}
		</select>
	</label><br>
	<button>Submit</button>
	`
);

const parse = parser(params => ({
	title: params.get('title'),
	parent: params.get('parent')
}));


export default async (req, user) => {
	if(req.method === 'POST') {
		const {title, parent} = await parse(req);
		await user.addTag(title, parent);
	}

	const tags = await user.getTags();
	const tagTree = await user.getTagTree();

	return html`
		${tree(tagTree)}
		${addTag(tags)}
	`;
};

function tree(tags){
	return html`<ul>
	${tags.map(t=>html`
		<li>
			${t.item.title} [${t.item.left},${t.item.right}]
			${t.children && tree(t.children)}
		</li>`)}
	</ul>`;
}
