import home from './home.js';

const user = {
	getLinks(){
		return [{
			url: 'http://example.com',
			title: 'Example site',
			description: 'Description'
		}]
	},
	getTagsFor(){
		return [{
			title: 'Tag'
		}]
	}
}

test('get home', async()=>{

	expect(await Promise.all((await home({}, user)).map(async v=>(await v).render()))).toMatchSnapshot();
});

test('post home', async()=>{
	const req = Object.assign(
		['name=paul'],
		{
			setEncoding: ()=>{},
			method: 'POST'
		}
	);

	expect(await Promise.all((await home(req, user)).map(async v=>(await v).render()))).toMatchSnapshot();
});
