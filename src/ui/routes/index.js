import html from 'encode-html-template-tag';
import home from './home.js';
import { redirect } from '../../rsvp/helpers.js';
import add from './add-link.js';
import tag from './tags.js';
import tagSearch from './tagSearch.js';
import es from 'expressive-switch';

const r = url => ({
	match: subject => url===subject,
	build:()=>url
});

const escapeRx = s=>s.replace(/[(){}*+?|^$.\\]/g, v=>`\\${v}`);
const namedGroup = name=>`(?<${name}>[^/]+)`;

function matcher(lits, ...args) {
	const rLits =lits.map(escapeRx);
	const grps = args.map(namedGroup);

	const matcher = new RegExp(rLits.reduce((exp, lit, ix)=>exp+grps[ix]+lit,'^'+rLits.shift()));

	return {
		match: u=>u.match(matcher)?.groups,
		build: params => lits.reduce((url, lit, ix)=>url+params[args[ix-1]]+lit),
		ennumerate: ()=>['a', 'b', 'c'].map(this.build)
	};
}


const p = {
	home: r('/'),
	login: r('/login'),
	add: r('/add'),
	tags: r('/tags'),
	tagSearch: matcher`/tags/${'tag'}`
};

export default ({auth}) => async (req, user) => {
	const { pathname } = new URL('file://' + req.url);

	const [s,c] = es();
	let args;
	switch(s(c=>args=c.match(pathname))) {
	case c(p.home):
		return home(req, user);
	case c(p.login):
		return redirect(await auth.getRedirectUrl('https://kith.kitchen'));
	case c(p.add):
		return add(req, user);
	case c(p.tags):
		return tag(req, user);
	case c(p.tagSearch):
		return tagSearch(req, user, args.tag);
	default:
		return html`Page not found`;
	}
};
