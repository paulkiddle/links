export default class User {
	#sql

	constructor(sql){
		this.#sql = sql;
	}

	async getLinks(options={}){
		const sql = this.#sql;

		if(options.tag) {
			return this.getLinksForTag(options.tag);
		}

		return sql`SELECT * FROM links`;
	}

	async getLinksForTag(tag){
		const sql = this.#sql;

		return sql`SELECT links.* FROM tags LEFT JOIN link_tags ON (tags.id=link_tags.tag_id) LEFT JOIN links ON (link_tags.link_id=links.id) WHERE tags.title=${tag}`;
	}

	async getTagsFor(linkId){
		const sql = this.#sql;

		return sql`SELECT tags.* FROM link_tags LEFT join tags on (link_tags.tag_id=tags.id) where link_tags.link_id=${linkId}`;
	}

	async getTags(){
		const sql = this.#sql;

		return sql`SELECT * FROM tags ORDER BY "left"`;
	}

	async getTagTree(){
		let node = {
			item: {
				left: -1,
				right: Infinity
			},
			children: []
		};
		const stack = [];

		for(const tag of await this.getTags()) {
			const child = { item: tag, children:[] };
			node.children.push(child);
			if(tag.right - tag.left > 1) {
				stack.push(node);
				node = child;
				continue;
			}
			let right = tag.right;
			while((node.item.right - right) <=1) {
				right = node.item.right;
				node = stack.pop();
			}
		}

		return node.children;
	}
}
