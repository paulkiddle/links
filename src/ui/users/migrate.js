import gm from 'good-migrations';
const migrate = gm.sql;

export default sql => {
	//const key = sql.MAX_VARIABLES ? sql=>sql`INTEGER PRIMARY KEY` : sql('INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY');

	if(sql.MAX_VARIABLES) {
		return migrate(sql, 'directory', [
			()=>sql`CREATE TABLE links (
				id INTEGER PRIMARY KEY,
				title VARCHAR,
				url VARCHAR UNIQUE,
				description TEXT,
				verified TIMESTAMP
			)`,
			()=>sql`CREATE TABLE tags (
				id INTEGER PRIMARY KEY,
				title VARCHAR UNIQUE,
				"left" INTEGER UNIQUE,
				"right" INTEGER UNIQUE
			)`,
			()=>sql`CREATE TABLE link_tags (
				id  NTEGER PRIMARY KEY,
				link_id INTEGER,
				tag_id INTEGER
			)`
		]);
	}

	return migrate(sql, 'directory', [
		()=>sql`CREATE TABLE links (
			id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
			title VARCHAR,
			url VARCHAR UNIQUE,
			description TEXT,
			verified TIMESTAMP
		)`,
		()=>sql`CREATE TABLE tags (
			id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
			title VARCHAR UNIQUE,
			"left" INTEGER UNIQUE,
			"right" INTEGER UNIQUE
		)`,
		()=>sql`CREATE TABLE link_tags (
			id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
			link_id INTEGER,
			tag_id INTEGER
		)`
	]);
};
