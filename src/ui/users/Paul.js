import User from './User.js';

export default class Paul extends User {
	isAuthenticated = true;

	name = 'Paul'

	#sql

	constructor(sql){
		super(sql);
		this.#sql = sql;
	}

	async addLink(link){
		const sql = this.#sql;

		await sql`INSERT INTO links (url, title, description, verified) VALUES(${link.href}, ${link.title}, ${link.description}, ${new Date})`;

		if(link.tags && link.tags.length) {
			const [{id}] = await sql`SELECT id FROM links WHERE url=${link.href}`;
			for(const tag of link.tags) {
				await sql`INSERT INTO link_tags (link_id, tag_id) VALUES (${id}, ${tag})`;
			}
		}
	}

	async addTag(tag, parent){
		const sql = this.#sql;

		if(parent) {
			const [{left}] = await sql`select "right" as "left" from tags where title=${parent} limit 1`;
			await sql`UPDATE tags SET "left"="left"+2 where "left" > ${left}`;
			await sql`UPDATE tags SET "right"="right"+2 where "right" >= ${left}`;
			await sql`INSERT INTO tags (title, "left", "right") VALUES(${[tag, left, left+1]})`;
		} else {
			const [{left}] = await sql`select max("right")+1 as "left" from tags`;

			await sql`INSERT INTO tags (title, "left", "right") VALUES(${[tag, left||0, (left||0)+1]})`;
		}
	}
}
