import Visitor from './Visitor.js';
import Paul from './Paul.js';
import migrate from './migrate.js';

export { migrate };

export default async (sql) => {
	await migrate(sql);

	const visitor = new Visitor(sql);
	const paul = new Paul(sql);

	return function getUser(user = {}) {
		if(user && user.sub === 'https://kith.kitchen/users/Paul'){
			return paul;
		} else {
			return visitor;
		}
	};
};
