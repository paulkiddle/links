import routes from './routes/index.js';
import router from './router.js';
import route from '../rsvp/route.js';
import JwtCookie from 'jwt-cookie';
import { redirect } from '../rsvp/helpers.js';
import UserFactory from './users/factory.js';

export default async ({sql, auth, jwtKey, authPath, ...options}) => {
	const jwt = new JwtCookie(jwtKey);
	const getUser = await UserFactory(sql);

	const r = router(route(routes({auth})), options);

	return async (req, res) => {
		const { pathname, searchParams } = new URL('file://' + req.url);
		if(pathname === authPath) {
			const code = searchParams.get('code');
			const issuer = searchParams.get('issuer');
			const user = await auth.getUserInfo(issuer.replace(/^(?![^:]+:)/, 'https://'), code);
			jwt.set(res, user);
			redirect('/')(res);
			res.end();
			return;
		}

		const user = getUser(jwt.get(req));

		return r(req, res, user);
	};
};
