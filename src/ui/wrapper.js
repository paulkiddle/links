import html from 'encode-html-template-tag';
import { page } from 'sp-templates';

export default function({user, ...options}, body){
	return page({
		...options,
		colour: 'red',
		controls:
			user.isAuthenticated ? html`${user.name}` : html`<a href="/login">Log in</a>`,
		menu: user.isAuthenticated ? [
			{ name: 'Home', href: '/' },
			{ name: 'Add link', href: '/add' },
			{ name: 'Add tag', href:'/tags'}
		] : undefined
	}, body);
}
