import html from './wrapper.js';

export default (app, options) => async function appRouter(req, res, user) {
	const response = await app(req, user);
	const body = response(res);

	if(body) {
		res.end(await html({ ...options, user }, body).render());
	} else {
		res.end();
	}

	return;
};
