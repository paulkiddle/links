function redirect(url, status=303) {
	return res => {
		res.statusCode = status;
		res.setHeader('location', url);
	};
}

export { redirect };
