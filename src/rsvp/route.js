function defaultError(e){
	console.error(e);
	return res => {
		res.statusCode = 500;
		return 'An error has occured.';
	};
}

export default function route(method, error=defaultError){
	return async function (...args) {
		try {
			const responder = await method(...args);
			if(responder instanceof Function) {
				return responder;
			}

			return (/*res*/)=>responder;
		} catch(e) {
			return error(e);
		}
	};
}
