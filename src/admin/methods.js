import Auth from 'masto-auth';

export default ({ origin, title, authPath }) => ({
	registerMastodon(instance = 'https://kith.kitchen') {
		return Auth.register(instance, {
			redirectUri: origin + authPath,
			clientName: title
		});
	}
});
