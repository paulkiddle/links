import route from '../../src/rsvp/route.js';
import { jest } from '@jest/globals';

test('Route wrapper', async()=>{
	const responder = ()=>{};
	const testRoute = jest.fn(()=>responder);

	const wrapped = route(testRoute);

	const res = await wrapped(1,2,3);

	expect(testRoute).toHaveBeenCalledWith(1,2,3);
	expect(res).toBe(responder);
});

test('Casts non-function response', async()=>{
	const responseBody = 'responseBody';
	const testRoute = jest.fn(()=>responseBody);

	const wrapped = route(testRoute);

	const res = await wrapped(1,2,3);

	expect(testRoute).toHaveBeenCalledWith(1,2,3);
	expect(res).toBeInstanceOf(Function);
	expect(res()).toBe(responseBody);
});

test('Catches errors', async()=>{
	const testRoute = jest.fn(()=>{
		throw new Error('test error');
	});

	const wrapped = route(testRoute);

	const res = await wrapped(1,2,3);

	expect(testRoute).toHaveBeenCalledWith(1,2,3);
	expect(res).toBeInstanceOf(Function);

	const o = {};
	const body = res(o);

	expect(o).toMatchSnapshot();
	expect(body).toMatchSnapshot();
});
