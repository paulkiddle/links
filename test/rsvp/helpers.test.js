import { redirect } from '../../src/rsvp/helpers.js';
import { jest } from '@jest/globals';

test('Redirect helper', async()=>{
	const res = {
		setHeader: jest.fn()
	};

	const url = 'http://example.com/home.html';
	const rtn = redirect(url, 319)(res);

	expect(rtn).toBe(void 0);
	expect(res.setHeader).toHaveBeenCalledWith('location', url);
	expect(res.statusCode).toBe(319);
});

test('Redirect helper default value', async()=>{
	const res = {
		setHeader: jest.fn()
	};

	const url = 'http://example.com/home.html';
	const rtn = redirect(url)(res);

	expect(rtn).toBe(void 0);
	expect(res.setHeader).toHaveBeenCalledWith('location', url);
	expect(res.statusCode).toBe(303);
});
