import JsonRpc from 'node-jsonrpc-client';

const client = new JsonRpc('http://localhost:8081');

const args = process.argv.slice(3).map(a=>{
	try {
		return JSON.parse(a);
	} catch(e) {
		return a;
	}
});

const {result, error} = await client.call(process.argv[2], args);

if(error) {
	console.error(error.message);
	process.exit(1);
}

console.log(result);
